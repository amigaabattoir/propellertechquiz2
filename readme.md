#Propeller Tech Quiz 2
Uses [flickr API](https://www.flickr.com/services/api/) to display 10 photos per page. When user clicks on the photo, it shows in an overlay. 

## Hosted Link
Hosted link [http://aws-website-propellertechquiz-e7vip.s3-website-us-east-1.amazonaws.com/](http://aws-website-propellertechquiz-e7vip.s3-website-us-east-1.amazonaws.com/)

## References
[Bootstrap 3](http://getbootstrap.com/)

Pagination from [jQuery pagination plugin (bootstrap powered)](https://github.com/esimakin/twbs-pagination)

API Help from [StackOverflow](http://stackoverflow.com/questions/2513417/jquery-getjson-how-do-i-parse-a-flickr-photos-search-rest-api-call)

Overlay from [Fancybox 2](http://fancyapps.com/fancybox/)